extends Resource

class_name HexMapResource


export(int) var map_width = 25
export(int) var map_height = 25
export(Array, Array, Dictionary) var data = []
