extends Resource

class_name SettingsResource


export(bool) var uninvert_mouse_y = true
export(bool) var sound_mute = false
export(bool) var fight_animations = true

