import random
from random import randrange
from random import random as randreal
from typing import Tuple, List


VERBOSE = True

def debug(*args):
    if VERBOSE:
        print(*args)


class Side:
    def __init__(self, name, troops, supplies):
        assert name in "abc"
        self.name = name
        self.troops = troops + supplies
        self.supplies = 0   # converted to troops at the start of combat, and any excess back to supply at the end
        self.original_troops = troops

class Battle:
    def __init__(self, a_side: Side, b_side: Side, tower: List):
        self.a_side = a_side
        self.b_side = b_side
        self.tower = tower
        self.new_tower = []

        self.rounds = 0
        
        self.checksum = self.get_total_troops()
        
        debug(f"New battle with {self.checksum} troops")
    
    def fight(self):
        "do a round of combat"
        self.rounds += 1
        
        troop_list = ['a'] * self.a_side.troops + ['b'] * self.b_side.troops
        random.shuffle(troop_list)
        
        for troop in troop_list:
            if troop == 'a':
                side = self.a_side
                enemy = self.b_side
            else:
                side = self.b_side
                enemy = self.a_side
                
            result = self.encounter(side, enemy)
            side.troops += result[0]
            enemy.troops += result[1]
            if result[2]:
                self.new_tower.append(side.name)
            if result[3]:
                self.new_tower.append(enemy.name)
            #debug(f"*** {result[2]} {result[3]}")
            
    def encounter(self, side, enemy) -> List:
        """send a troop from a side on an encounter
        
        Returns an array of results = [
            change to friendly troop count: int,
            change to enemy troop count: int,
            add a friendly supply: bool,
            add an enemy supply: bool
        ]
        """

        def encounter_result():
            # each troop has a 1/3 chance to just do nothing each round
            if randrange(3) == 0:
                return "nothing"
            
            # each troop has a sliding chance to either encounter supplies, or get lost and turned into supplies themselves
            # the chances depend on how much supply is in the tower
            #   2 or less supply is 1/3 encounter, 1/4 lost
            #   12 supply+ is 2/3 encounter, 1/6 lost
            population = min((self.get_total_supplies() - 2) / 10, 1.0)
            lost_chance = 1 / (4 + (3 * population))
            encounter_chance = (1/3) + ((1/3) * population) 
            
            roll = randreal()
            if roll <= lost_chance:
                return "lost"
            elif roll <= lost_chance + encounter_chance:
                # adventure!
                if self.get_total_supplies() != 0:
                    return "adventure"
                else:
                    return "nothing"
            else:
                return "nothing"
        
        result = [0, 0, 0, 0]
        hero = side.name
        
        while hero:
            x = encounter_result()
            debug(side.name, hero, x)
            if x == "nothing":
                hero = None
            elif x == "lost":
                if hero == side.name:
                    result[0] -= 1
                    result[2] += 1
                elif hero == enemy.name:
                    result[1] -= 1
                    result[3] += 1
                hero = None
            elif x == "adventure":
                # bump into a random troop, and that troop does a single encounter
                found = random.choice(self.tower)
                hero = found
                if found == side.name:
                    debug("found friend")
                    result[0] += 1
                    self.tower.remove(found)
                elif found == enemy.name:
                    debug("found enemy")
                    result[1] += 1
                    self.tower.remove(found)
                else:
                    # 3rd party, leave the supply in the chain
                    debug("found neutral")
                    
        return result

    def get_total_supplies(self):
        return len(self.tower)
    
    def get_total_troops(self):
        "checksum of every troop in the system"
        return len(self.tower) + len(self.new_tower) + self.a_side.troops + self.b_side.troops + self.a_side.supplies + self.b_side.supplies
    
    def resolve(self):
        "end the fight"
        a_total = self.a_side.troops
        b_total = self.b_side.troops
        lost = min(a_total, b_total)
        
        a_survived = a_total - lost
        b_survived = b_total - lost
        self.a_side.troops = a_survived
        self.a_side.supplies = 0
        self.b_side.troops = b_survived
        self.b_side.supplies = 0
        
        if a_survived >= self.a_side.original_troops:
            self.a_side.supplies = a_survived - self.a_side.original_troops
            self.a_side.troops = self.a_side.original_troops
        if b_survived >= self.b_side.original_troops:
            self.b_side.supplies = b_survived - self.b_side.original_troops
            self.b_side.troops = self.b_side.original_troops
            
        checksum_verify = self.get_total_troops() + 2*lost
        
        debug(f"fought {self.rounds} rounds, both sides lost {lost}, added {''.join(self.new_tower)} to supplies, total {checksum_verify}")
        assert checksum_verify == self.checksum
        self.tower.extend(self.new_tower)
        self.new_tower = []
            
        
        
    def summary(self):
        n = self.new_tower
        a = self.a_side
        b = self.b_side
        t = self.tower
        retval = []
        retval.append(f"{a.name}: {a.troops}+{a.supplies}")
        retval.append(f"{b.name}: {b.troops}+{b.supplies}")
        retval.append("|")
        retval.append(f"({len(t)}):")
        retval.append("".join(t))
        retval.append("|")
        retval.append(f"({len(n)}):")
        retval.append("".join(n))
        retval.append(f"= {self.get_total_troops()}")
        
       
        debug(" ".join(retval))



def main():
    global VERBOSE
    random.seed()
    
    tower = ['a'] * 3 + ['b'] * 3 + ['c'] * 6
    
    a = Side("a", 3, 1)
    b = Side("b", 3, 0)
    
    battle = Battle(a, b, tower)
    battle.summary()
    battle.fight()
    battle.summary()
    battle.fight()
    battle.summary()
    battle.fight()
    battle.summary()
    # 
    print("---")
    battle.resolve()
    tower.sort()
    battle.summary()
    
    print("SUCCESS!")
    
    debug()
    print("Run some fights")
    VERBOSE = False
    wins = [0,0]
    
    round_count = 2000
    for i in range(round_count):
        tower = ['a'] * 3 + ['b'] * 3 + ['c'] * 6
        a = Side("a", 3, 1)
        b = Side("b", 3, 0)
        battle = Battle(a, b, tower)
        battle.fight()
        battle.fight()
        battle.fight()
        battle.fight()
        battle.resolve()
        if a.troops > b.troops:
            wins[0] += 1
        elif a.troops < b.troops:
            wins[1] += 1
    print(f"Results of fights: {wins} A:{wins[0] / round_count * 100:.1f}% A:{wins[1] / round_count * 100:.1f}% Draw:{(round_count - wins[0] - wins[1]) / round_count * 100:.1f}%")
        





if __name__ == "__main__":
    main()
