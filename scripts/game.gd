extends Node

var Player := preload("res://scripts/player.gd")
var Army := preload("res://scenes/army.tscn")
var Battle := preload("res://scripts/battle.gd")
var SupplyChain := preload("res://scripts/supplychain.gd")
var AITool := preload("res://scripts/aitool.gd")

const STATE = GameDefs.State
const MESSAGE = GameDefs.Message
const HEX_SPEC = GameDefs.HexSpec

signal update_status	# (s:str)
signal update_title		# (s:str)
signal request_value 	# {"title":str, "body":str, "min":int, "max":int}
signal request_assignment	# {"title":str, "options":Array}
signal assign_coords	# (coords:Array)

# client state details
var client_state: int = STATE.NOTHING
var client_state_details: ClientState
var client_active_player
var client_queued_players = []
var server_state: int = STATE.NOTHING

# server state details
var game_state: int = STATE.NOTHING

var active_player
var _player_order: Array
var _active_player_index: int

var active_coords
var active_target_coords
var active_value
var state_helper

# our world
var players = []
var armies = {}		# Dict of armies stored by the hex coords they occupy
var chain
var turn_count: int = 0

var _hexgrid: HexGrid
var _hexmap: HexMap
var _gui: Node

func start_game(hexgrid, gui):
	# set up our internals
	_gui = gui
	_hexgrid = hexgrid
	_hexmap = hexgrid.hexmap
	chain = SupplyChain.new()
	chain.connect("change_supplies", gui, "_on_supplies_changed")
	
	gui.connect("return_value", self, "_on_gui_return_value")
	gui.connect("return_assignment", self, "_on_gui_return_assignment")
	
	# create things
	var p1 = create_player(1, "Player 1", preload("res://sprites/neato.tres"))
	var p2 = create_player(2, "Player 2", preload("res://sprites/tbj2_all.tres"))
	var p3 = create_player(3, "Player 3", preload("res://sprites/greendude.tres"))
	var p4 = create_player(4, "Player 4", preload("res://sprites/bluedude.tres"))
	var p5 = create_player(5, "Neutral", preload("res://sprites/chicken.tres"))
	
	chain.set_supplies([p1, p1, p1, p1, p2, p2, p2, p2, p3, p3, p3, p3, p4, p4, p4, p4])
	
	p1.is_local = true
	p5.is_neutral = true

	# random start for testing
	# var all_land = Game.get_all_usable_land()
	# all_land.shuffle()
	# for p in players:
	# 	if p.is_neutral:
	# 		continue

	# 	for i in range(len(GameDefs.start_armies)):
	# 		var _test_army = create_army(p, all_land.pop_back(), 2, 0)

	# for coords in _hexmap.node_at_coords:
	# 	var hex_data = _hexmap.get_hex_at(coords)
	# 	if hex_data["elevation"] == 7:
	# 		_test_army = create_army(players[0], coords, 2, 1)
	# 	elif hex_data["elevation"] == 5:
	# 		_test_army = create_army(players[0], coords, 0, randi() % 2 + 1)
	# 	elif hex_data["elevation"] == 6:
	# 		_test_army = create_army(players[1], coords, randi() % 2 + 1, randi() % 2 + 1)
	
	# kick the game
	change_game_state(STATE.START)


func create_player(p_player_id: int, p_name: String, p_frames: SpriteFrames, money: int = 3):
	print("Creating player '%s'" % p_name)
	var player = Player.new(p_player_id, p_name, p_frames, money)
	players.append(player)
	return player

func get_human_players():
	var retval = []
	for player in players:
		if not player.is_neutral:
			retval.append(player)
	return retval

func create_spriteflag(coords, p_texture, sentinel, death_signal):
	print("Create a temp visual flag that lives until a signal is received")
	var flag = preload("res://scenes/spriteflag.tscn").instance()
	flag.texture = p_texture
	flag.origin = _hexgrid.grid2_to_world(coords)
	flag.viewport = _hexgrid.get_viewport()
	sentinel.connect(death_signal, flag, "_die1")
	_gui.add_child(flag)

func create_army(player, coords: Array, troops: int, supply: int = 0)->Army:
	print("Creating army (%d, %d) at %s" % [troops, supply, coords])
	var army = Army.instance()
	army.player = player
	army.frames = player.frames
	army.resize(troops, supply)
	armies[coords] = army
	_hexgrid.show_sprite(army, coords)
	return army

func move_army(from, to, troops:int):
	"move the army at 'from' coords to the 'to' coords"
	# TODO: trigger some rotate maybe
	var army: Army = armies[from]
	print("Moving army to %s" % [to])
	
	# gather the supplies at destination
	var new_supply: int = 0
	var new_troops: int = troops
	var current_supplies = army.get_supply()
	var current_troops = army.get_troops()
	if to in armies:
		new_supply = armies[to].get_supply()
		new_troops = new_troops + armies[to].get_troops()
		armies[to].destroy()

	# are we leaving anyone behind?
	var garrison_troops = army.get_troops() - troops
	
	# set off
	armies[to] = army
	armies.erase(from)

	# build our garrison
	if (garrison_troops > 0) or (current_supplies > 0):
		create_army(army.player, from, garrison_troops, current_supplies)
	
	# adjust the current army
	if (current_troops != new_troops) or (current_supplies != new_supply):
		army.resize(new_troops, new_supply)
	
	_hexgrid.move_sprite(army, to)
	print("move over")

func merge_army(from, to, troops:int):
	"add the army at 'from' coords to the army at the 'to' coords"
	# this is all able to fit in the move_army() right now
	move_army(from, to, troops)

func enact_fight(from, to, results):
	"The army at 'from' attacks the army at 'to'"
	var a_army: Army = armies[from]
	var b_army: Army = armies[to]
	var final_chain: Array = chain.to_array()

	# start the animation
	if GameDefs.fight_animations:
		yield(a_army.animate_fight(b_army, results, chain), "completed")

	print("resuming fight recreation")

	# work out the victors
	if results[0] + results[1] == 0:
		print("attacker destroyed")
		armies.erase(from)
	if results[2] + results[3] == 0:
		print("defender destroyed")
		armies.erase(to)
	
	a_army.resize(results[0], results[1])
	b_army.resize(results[2], results[3])
	
	# make the supply chain match our canonical record
	for supplies in results[4]:
		final_chain.append(supplies)
	for supplies in results[5]:
		final_chain.erase(supplies)
	chain.set_supplies(final_chain)

	print("Fight over! %s should have %d troops and %d supplies, %s should have %d troops and %d supplies." %[
			a_army.player.name, results[0], results[1], b_army.player.name, results[2], results[3]
		])
	print("Supplies: %d" % [chain.size()])
	Network.send_server_message(MESSAGE.FIGHT_COMPLETE, {})


func get_army_coords(army: Army):
	"Returns the 2coords for a supplied Army object"
	for coords in armies:
		if armies[coords] == army:
			return coords


func get_all_usable_land()->Array:
	"return all usable hex coords"
	var retval := []
	for coords in _hexmap.node_at_coords:
		var hex_data = _hexmap.get_hex_at(coords)
		if (hex_data["elevation"] < 7) and (hex_data["elevation"] >= 2):
			retval.append(coords)

	return retval

func new_round(p_player_order=null):
	"Start a new round of player activities"
	if p_player_order == null:
		p_player_order = players

	_player_order = p_player_order.duplicate()
	_active_player_index = -1
	advance_player()
	return active_player

func advance_player():
	"get the next player in our turn order"
	var new_active_player = null
	while _active_player_index < (len(players)-1):
		_active_player_index += 1
		if _player_order[_active_player_index].is_neutral:
			continue
		new_active_player = _player_order[_active_player_index]
		break

	if new_active_player == null:
		print("next_player(): round over!")

	active_player = new_active_player

func get_player_by_id(player_id):
	for player in players:
		if player.player_id == player_id:
			return player

func victory_check():
	# for now we just check for total domination
	var alive_players := {}
	for key in armies:
		var army = armies[key]
		if army.get_troops() > 0:
			alive_players[army.player] = true
	
	if len(alive_players) <= 1:
		print("Game is over")
		emit_signal("update_status", "Game is over")	# server can't really do this
		return true
	return false

func send_client_message(message_type: int, active_player_id:int, details: Dictionary):
	# wrapper around Network.send_client_message
	Network.send_client_message(0, game_state, message_type, active_player_id, details)

func click_area(area: InteractArea):
	# abstract away the reliance on area, we don't actually care about Areas now
	if not area:
		# clicked nowhere
		return
	else:
		return click(area.coords)

func click(coords: Array):
	# do different things based on client state
	match client_state:
		STATE.NOTHING:
			print("Client state is 'NOTHING'")
		STATE.SELECT_HEX, STATE.SELECT_ASSIGNMENT:
			# server has requested we select a hex of some sort
			if not client_active_player.is_local:
				# not our turn
				return

			match client_state_details.details["hex_spec"]:
				HEX_SPEC.ANY:
					if coords in client_state_details.details["coords"]:
						change_client_state(STATE.WAITING)
						Network.send_server_message(MESSAGE.SUBMIT_HEX, {"coords": coords})

				HEX_SPEC.NEIGHBOR:
					var current_node = _hexmap.node_at_coords[client_state_details.details["coords"]]
					var target_node = _hexmap.node_at_coords[coords]
					if (current_node != target_node):
						if _hexmap.graph.are_points_connected(current_node, target_node):
							emit_signal("update_status", "")
							change_client_state(STATE.WAITING)
							Network.send_server_message(MESSAGE.SUBMIT_HEX, {"coords": coords})
				HEX_SPEC.FRIENDLY_ARMY:
					click_friendly_army(coords)

func click_friendly_army(coords):
	if not coords in armies:
		return

	var army = armies[coords]
	if (army) and (army.get_troops() > 0) and (army.player == client_active_player):
		# selected a vaild troop!
		match client_state:
			STATE.SELECT_HEX:
				emit_signal("update_status", "")
				change_client_state(STATE.WAITING)
				Network.send_server_message(MESSAGE.SUBMIT_HEX, {"coords": coords})

			STATE.SELECT_ASSIGNMENT:
				var option = _gui.assign_to_selected(coords)
				
				if option != null:
					# flag the location
					create_spriteflag(coords, option.icon, option, "assignment_cleared")


func _on_gui_return_value(val):
	print("Got return value from GUI: %s" % [str(val)])

	match client_state:
		STATE.SELECT_VALUE:
			emit_signal("update_status", "")
			change_client_state(STATE.WAITING)
			Network.send_server_message(MESSAGE.SUBMIT_VALUE, {"value": val})

func _on_gui_return_assignment(val):
	print("Got return assignment from GUI: %d options" % [len(val)])

	match client_state:
		STATE.SELECT_ASSIGNMENT:
			emit_signal("update_status", "")
			change_client_state(STATE.WAITING)
			Network.send_server_message(MESSAGE.SUBMIT_ASSIGNMENT, {"value": val})

			# check for simultaneous turns
			if len(client_queued_players) > 0:
				client_assignment()


func change_game_state(new_state: int, details=null):
	print("change_game_state %d %s" % [new_state, STATE.keys()[new_state]])
	assert(new_state in STATE.values())
	
	var next_state = null	# for chaining to another state
	var _last_state := game_state # currently unused
	
	game_state = new_state
	
	# actions when starting a state	
	match game_state:
		STATE.START:
			# clear our state, this should be moved to "NEXT_PHASE" state later
			active_coords = null
			active_target_coords = null
			active_value = null

			send_client_message(MESSAGE.START, 0, {"message": "Game starting!"})
			next_state = STATE.OFFER_LAND

		STATE.OFFER_LAND:
			state_helper = preload("res://scripts/state_offer_land.gd").new()
			state_helper._start()
		STATE.PLACE_ARMY:
			state_helper = preload("res://scripts/state_place_army.gd").new()
			state_helper._start()
		STATE.NEW_ROUND:
			# clear our state
			active_coords = null
			active_target_coords = null
			active_value = null

			state_helper = preload("res://scripts/state_new_round.gd").new()
			state_helper._start()

			
		STATE.NEW_PHASE:
			# clear our state
			active_coords = null
			active_target_coords = null
			active_value = null

			state_helper = preload("res://scripts/state_new_phase.gd").new()
			state_helper._start()


		STATE.SELECT_HEX:
			send_client_message(MESSAGE.REQUEST_HEX, active_player.player_id, {
				"hex_spec": HEX_SPEC.FRIENDLY_ARMY,
				"coords": null
			})
			
		STATE.SELECT_NEIGHBOR:
			send_client_message(MESSAGE.REQUEST_HEX, active_player.player_id, {
				"hex_spec": HEX_SPEC.NEIGHBOR,
				"coords": active_coords
			})

		STATE.SELECT_VALUE:
			send_client_message(MESSAGE.REQUEST_VALUE, active_player.player_id, 
				details
			)
		
		STATE.MOVE:
			send_client_message(MESSAGE.MOVE, active_player.player_id, {
				"from": active_coords,
				"to": active_target_coords,
				"troops": active_value,
			})
			next_state = STATE.SELECT_HEX
		
		STATE.MERGE:
			send_client_message(MESSAGE.MERGE, active_player.player_id, {
				"from": active_coords,
				"to": active_target_coords,
				"troops": active_value,
			})
			next_state = STATE.SELECT_HEX
		
		STATE.FIGHT:
			# do the fight
			var a_army: Army = armies[active_coords]
			var b_army: Army = armies[active_target_coords]
			var battle = Battle.new(a_army.player, b_army.player, a_army, b_army, chain)
			var results = battle.do_fight()
			
			# send the result
			send_client_message(MESSAGE.FIGHT_RESULT, active_player.player_id, {
				"from": active_coords,
				"to": active_target_coords,
				"results": results,
			})

			# wait for clients to be ready
			# TODO
			
			if victory_check():
				next_state = STATE.GAME_OVER
			else:
				next_state = STATE.SELECT_HEX
		_:
			print("UNHANDLED GAME STATE CHANGE %d %s" % [ game_state, STATE.keys()[game_state] ])
	
	# queue the next state (if applicable)
	if next_state != null:
		assert(next_state in GameDefs.State.values())
		call_deferred("change_game_state", next_state)


func server_submit_hex(details):
	# TODO validate the client responses
	match game_state:
		STATE.SELECT_HEX:
			# chose army
			active_coords = details["coords"]
			call_deferred("change_game_state", STATE.SELECT_NEIGHBOR)
		STATE.SELECT_NEIGHBOR:
			# chose where to go
			active_target_coords = details["coords"]

			# check if we need more info
			if armies[active_coords].get_troops() == 1:
				# only one troop to move
				active_value = 1
				server_prepare_move_old()
			else:
				# request move details
				var value_details = {
					"title": "Army Movement",
					"body": "Move how many troops?",
					"min": 1,
					"max": armies[active_coords].get_troops(),
				}

				call_deferred("change_game_state", STATE.SELECT_VALUE, value_details)

func server_prepare_move_old():
	"this func works out what kind of move to do. It expects all the active_* state to exist."
	if (active_target_coords in armies):
		if armies[active_target_coords].get_troops() == 0:
			# pick up the supplies
			call_deferred("change_game_state", STATE.MOVE)
		elif armies[active_target_coords].player != active_player:
			# fight the enemy troops
			call_deferred("change_game_state", STATE.FIGHT)
		else:
			# we are joining two armies
			call_deferred("change_game_state", STATE.MERGE)
	else:
		# move to empty hex
		call_deferred("change_game_state", STATE.MOVE)

func server_prepare_move(move_from, move_to, troops: int):
	"this func works out what kind of move to do"

	var details = {"from": move_from, "to": move_to, "troops": troops}

	if (move_to in armies):
		if armies[move_to].get_troops() == 0:
			# pick up the supplies
			call_deferred("send_client_message", MESSAGE.MOVE, active_player.player_id, details)
			# send_client_message(MESSAGE.MOVE, active_player.player_id, details)
		elif armies[move_to].player != active_player:
			# fight the enemy troops
			server_prepare_fight(move_from, move_to, troops)
		else:
			# we are joining two armies
			# send_client_message(MESSAGE.MERGE, active_player.player_id, details)
			call_deferred("send_client_message", MESSAGE.MERGE, active_player.player_id, details)

	else:
		# move to empty hex
		call_deferred("send_client_message", MESSAGE.MOVE, active_player.player_id, details)

func server_prepare_fight(move_from, move_to, troops: int):
	# do the fight
	var a_army: Army = armies[move_from]
	var b_army: Army = armies[move_to]
	var battle = Battle.new(a_army.player, b_army.player, a_army, b_army, chain)
	var results = battle.do_fight()

	# send the result
	send_client_message(MESSAGE.FIGHT_RESULT, active_player.player_id, {
		"from": move_from,
		"to": move_to,
		"results": results,
	})

func server_submit_value(details):
	# TODO validate the client responses
	active_value = details["value"]
	match game_state:
		STATE.SELECT_VALUE:
			server_prepare_move_old()

func change_client_state(new_state: int):
	print("change_client_state %d:%s -> %d:%s" % [
		game_state, STATE.keys()[game_state], 
		new_state, STATE.keys()[new_state],
		])
	assert(new_state in STATE.values())
	
	var next_state = null	# for chaining to another state
	var last_state := game_state
	
	client_state = new_state
	
	# actions when leaving a state
	match last_state:
		STATE.SELECT_HEX:
			_hexgrid.clear_highlight()
		STATE.SELECT_NEIGHBOR:
			_hexgrid.clear_highlight()
		_:
			_hexgrid.clear_highlight()
	
	# actions when starting a state	
	match client_state:
		STATE.WAITING:
			update_gui_status("waiting...")
#		_:
#			print("UNHANDLED CLIENT STATE CHANGE %d %s" % [ game_state, STATE.values()[game_state] ])
	
	# queue the next state (if applicable)
	if next_state != null:
		assert(next_state in GameDefs.State.values())
		call_deferred("change_game_state", next_state)
		

func client_start():
	emit_signal("update_status", client_state_details.details["message"])
	change_client_state(STATE.WAITING)
	pass

func client_select_hex():
	var details = client_state_details.details
	var hex_spec = details["hex_spec"]
	assert(hex_spec in HEX_SPEC.values())

	change_client_state(STATE.SELECT_HEX)

	match hex_spec:
		HEX_SPEC.ANY:
			if server_state == STATE.OFFER_LAND:
				update_gui_status("select starting locations to claim, each player takes turns until they have 8", true)
				
				# Autoselect land for faster set up
				if GameDefs.auto_pick_land:
					call_deferred("click", 
						AITool.auto_pick_land(
							client_active_player, 
							details["coords"], 
							armies, 
							_hexmap
						)
					)

			else:
				update_gui_status("select a location", true)

			for coords in details["coords"]:
				_hexgrid.highlight_hex(coords)
			
		HEX_SPEC.NEIGHBOR:
			var hint = details.get("hint", "select an adjacent location")
			update_gui_status(hint, true)
			_hexgrid.highlight_adjacent(details["coords"])
			
		HEX_SPEC.FRIENDLY_ARMY:
			update_gui_status("select a friendly army", true)
			_hexgrid.highlight_player_armies(client_active_player, armies)

			if server_state == STATE.PLACE_ARMY:
				# Autoselect army placement for faster set up
				if GameDefs.auto_place_armies:
					call_deferred("click", 
						AITool.auto_pick_land(
							client_active_player, 
							details["coords"], 
							armies, 
							_hexmap
						)
					)


func client_select_value():
	# request the UI give us a value
	change_client_state(STATE.SELECT_VALUE)
	update_gui_status("select a value", true)
	emit_signal("request_value", client_state_details.details)

func client_assignment():
	# assign each of a set to unique hexes
	var details = client_state_details.details
	var hex_spec = details["hex_spec"]
	assert(hex_spec in HEX_SPEC.values())

	# assignment support simultaneous turns
	if len(client_queued_players) > 0:
		client_active_player = client_queued_players.pop_front()
		update_gui_title()

	change_client_state(STATE.SELECT_ASSIGNMENT)
	match server_state:
		STATE.PLACE_ARMY:
			update_gui_status("place starting armies")
		STATE.NEW_ROUND:
			update_gui_status("planning")
		_:
			update_gui_status("assign options", true)

	match hex_spec:
		HEX_SPEC.FRIENDLY_ARMY:
			_hexgrid.highlight_player_armies(client_active_player, armies)

	# call the assignment UI
	var gui_title = "assign"
	if "title" in details:
		gui_title = details["title"]
	emit_signal("request_assignment", gui_title, details["options"])

func client_money():
	# give a player money
	var details = client_state_details.details
	print("Giving %s %d money" % [active_player.name, details["money"]])
	active_player.money += details["money"]

func client_deploy():
	# give a player money
	var details = client_state_details.details
	var coords = details["coords"]
	var deploy = details["deploy"]
	var army = armies[coords]
	print("Giving %s %d troops" % [active_player.name, details["deploy"]])
	army.resize(army.get_troops() + deploy, army.get_supply())

func client_enact_fight():
	emit_signal("update_status", "There's a fight happening")
	change_client_state(STATE.FIGHT)
	enact_fight(
		client_state_details.details["from"],
		client_state_details.details["to"],
		client_state_details.details["results"]
	)
	emit_signal("update_status", "")
	change_client_state(STATE.WAITING)
	
func client_move_army():
	emit_signal("update_status", "Moving army")
	move_army(
		client_state_details.details["from"], 
		client_state_details.details["to"],
		client_state_details.details["troops"]
	)
	emit_signal("update_status", "")
	change_client_state(STATE.WAITING)
	Network.send_server_message(MESSAGE.MOVE_COMPLETE, {})

	
func client_merge_army():
	emit_signal("update_status", "Merging army")
	merge_army(
		client_state_details.details["from"],
		client_state_details.details["to"],
		client_state_details.details["troops"]
	)
	emit_signal("update_status", "")
	change_client_state(STATE.WAITING)
	Network.send_server_message(MESSAGE.MOVE_COMPLETE, {})


func update_gui_title(title:String=""):
	"update our title text based on client knowledge"
	if title:
		emit_signal("update_title", title)
	else:
		# smart work out our title
		if client_active_player == null:
			title = "Welcome to " + ProjectSettings.get_setting('application/config/name')
		else:
			title = client_active_player.name + "'s turn"
		emit_signal("update_title", title)

func update_gui_status(body:String, target:bool=false):
	"update our status text, optionally targetting it to the local player"
	if target:
		if not client_active_player.is_local:
			body = "Waiting for %s to %s" % [client_active_player.name, body]	

	emit_signal("update_status", body)
