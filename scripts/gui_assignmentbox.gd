extends Control

export(NodePath) var title_label_path
export(NodePath) var option_container_path

signal option_hint

var active_option = null
var require_distinct = true

var _assign_option_template = preload("res://scenes/gui_assignoption.tscn")


func _ready():
	show_window(false)


func show_window(show: bool, title: String="", options:Array=[], p_require_distinct:bool=true):
	if show:
		var option_container = get_node(option_container_path)
		var title_label = get_node(title_label_path)

		active_option = null
		title_label.text = title.left(7)
		require_distinct = p_require_distinct

		# clean up old buttons
		for button in option_container.get_children():
			option_container.remove_child(button)
			button.queue_free()
			if button.assigned_value:
				button.assigned_value = null

		# create the option buttons
		for item in options:
			# [option_id, icon, text (6 chars), hint text]
			var button = _assign_option_template.instance()
			button.option_id = item[0]
			button.icon = item[1]
			button.text = item[2]
			button.status_hint = item[3]

			option_container.add_child(button)
			button.connect("option_selected", self, "_on_option_selected")
	else:
		# going away, clear our selections
		var option_container = get_node(option_container_path)
		for button in option_container.get_children():
			if button.assigned_value:
				button.assigned_value = null

	visible = show

func get_assignment():
	"return our complete list of assignments"
	var retval = {}
	var option_container = get_node(option_container_path)
	for node in option_container.get_children():
		retval[node.option_id] = node.assigned_value
	return retval			

func assign_to_selected(val):
	"""assign a value to whatever option is selected

	Returns the node that accepted the assignment, or null if assignment fails
	"""
	if (active_option == null) or not active_option.get_is_pressed():
		return null

	# check if this is a distinct value
	if require_distinct:
		var option_container = get_node(option_container_path)
		for node in option_container.get_children():
			if node.assigned_value == val:
				emit_signal("option_hint", "can't assign the same thing to two options")
				return null

	# assign
	active_option.assigned_value = val
	return active_option

func _on_option_selected(button):
	# disable the previous button
	if (active_option != null) and (active_option != button):
		active_option.get_node("Toggle").pressed = false

	active_option = button
	emit_signal("option_hint", button.status_hint)

	
