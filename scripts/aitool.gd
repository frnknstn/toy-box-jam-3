extends Reference

"AI routines and helpers"

var foo: int


static func get_adjacent_armies(coords, player, armies: Dictionary, hexmap) -> int:
	"""get all the adjacent armies belonging to a player, or all players if null"""
	var retval = []
	var neighbors = hexmap.hex_connected_neighbors(coords)
	for neighbor_coords in neighbors:
		if neighbor_coords in armies:
			var army = armies[neighbor_coords]
			if not player or army.player == player:
				retval.append(army)
	return retval

static func auto_pick_land(player, land_shop: Array, armies: Dictionary, hexmap):
	"""pick the hex that is most adjacent to our other armies, preferring the newest"""
	var max_neighbors: int = 0
	var best_land = land_shop[0]
	
	for coords in land_shop:
		var count: int = len(get_adjacent_armies(coords, player, armies, hexmap))
		print("%s has %d friendly neighbors" % [coords, count])
	
		if count >= max_neighbors:
			# if we are the same but newer, prefer us
			max_neighbors = count
			best_land = coords

	return best_land

# static func auto_place_army(player, armies: Dictionary, hexmap):
# 	"""Automatically bolster your armies"""

# 	# policy[friendly_neighbors][enemy_neighbours]
# 	policy = [
# 		# no friends adjacent
# 		[1.0, 2.5, 3.5, 3.0, 2.5, 1.0, 0.5],
# 		# 1 ally
# 		[1.2, 2.5, 3.5, 3.0, 2.5, 2.0],
# 		# 2 allies
# 		[1.4, 2.0, 1.8, 1.6, 1.0],
# 		# 3 allies
# 		[1.1, 1.9, 3.1, 3.2], 
# 		# 4 allies
# 		[0.7, 2.5, 2.8],
# 		# 5 allies
# 		[0.8, 2.1],
# 		# 6 allies
# 		[1.8]
# 	]

# 	var friends = get_adjacent_armies(coords, player, armies, hexmap)
# 	var enemies = get_adjacent_armies(coords, null, armies, hexmap)





# 	GameDefs.start_armies




func _init(p_state:int, p_active_player_id: int, p_details: Dictionary):
	print("AI Tool init")
