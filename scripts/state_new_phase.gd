extends Node

"This state is for requesting actions from players"

const STATE = GameDefs.State
const MESSAGE = GameDefs.Message
const HEX_SPEC = GameDefs.HexSpec
const ACTION = GameDefs.Action

var phases: Array
var current_phase: int

var move_from
var move_to
var move_troops
var move_left_troops

func _start():
	"called when entering a state"

	phases = GameDefs.action_options.duplicate()
	next_phase()
	

func next_phase():
	if len(phases) == 0:
		# phases over
		var over = scoring()
		if not over:
			Game.call_deferred("change_game_state", STATE.NEW_ROUND)
		return

	current_phase = phases.pop_front()[0]
	print("Moving to action phase %d:%s" % [current_phase, ACTION.keys()[current_phase]])
	Game.new_round()
	do_player_phase()


func do_player_phase():
	var coords
	var player = Game.active_player
	var wait_for_reply = false

	# clean up phase state
	move_from = null
	move_to = null
	move_troops = null
	move_left_troops = null

	# check a the player assigned the action, and still owns the coords
	if player.action_plan[current_phase] == null:
		print("Player did not assign action")
		end_player_phase()
		return

	coords = player.action_plan[current_phase]
	if not (coords in Game.armies):
		print("Nobody controls planned region")
		end_player_phase()
		return

	if Game.armies[coords].player != player:
		print("Player no longer controls planned region")
		end_player_phase()
		return

	#we are good to go
	match current_phase:
		ACTION.MONEY:
			Game.send_client_message(MESSAGE.MONEY, player.player_id, {
				"money": 6,
			})
		ACTION.DEPLOY_5:
			if player.money < 3:
				print("Player cannot afford to deploy 5 troops")
			else:
				Game.send_client_message(MESSAGE.MONEY, player.player_id, {
					"money": -3,
				})
				Game.send_client_message(MESSAGE.DEPLOY, player.player_id, {
					"coords": coords,
					"deploy": 5,
				})
		ACTION.DEPLOY_3:
			if player.money < 2:
				print("Player cannot afford to deploy 3 troops")
			else:
				Game.send_client_message(MESSAGE.MONEY, player.player_id, {
					"money": -2,
				})
				Game.send_client_message(MESSAGE.DEPLOY, player.player_id, {
					"coords": coords,
					"deploy": 3,
				})
		ACTION.ATTACK_A, ACTION.ATTACK_B:
			# request a target place
			move_from = coords
			Game.send_client_message(MESSAGE.REQUEST_HEX, player.player_id, {
				"hex_spec": HEX_SPEC.NEIGHBOR,
				"coords": move_from,
				"hint": "select a location to attack or merge with",
			})
			wait_for_reply = true		
	
	if not wait_for_reply:
		# no waiting for a response here
		end_player_phase()


func end_player_phase():
	# end the phase
	Game.advance_player()
	if Game.active_player == null:
		next_phase()
		return
	call_deferred("do_player_phase")




func _recv(message_type: int, details: Dictionary, player=null):
	"called when we get a message"

	match message_type:
		MESSAGE.SUBMIT_HEX:
			recv_hex(details)
		MESSAGE.SUBMIT_VALUE:
			recv_value(details)
		MESSAGE.MOVE_COMPLETE:
			# TODO: in the future, we need a better way of doing this with MP
			end_player_phase()
		MESSAGE.FIGHT_COMPLETE:
			# TODO: in the future, we need a better way of doing this with MP
			if move_from in Game.armies:
				var army = Game.armies[move_from]
				move_troops = army.get_troops() - move_left_troops
				# Game.call_deferred("server_prepare_move", move_from, move_to, move_troops)
				Game.server_prepare_move(move_from, move_to, move_troops)
			else:
				end_player_phase()

		_:
			print("UNHANDLED SERVER MESSAGE %d %s" % [ message_type, MESSAGE.keys()[message_type] ])
			assert(false)



func recv_hex(details: Dictionary):
	match current_phase:
		ACTION.ATTACK_A, ACTION.ATTACK_B:
			move_to = details["coords"]

			# check if we need more info
			if Game.armies[move_from].get_troops() == 1:
				# only one troop to move
				move_troops = 1
				move_left_troops = 0
				Game.server_prepare_move(move_from, move_to, move_troops)
			else:
				# request move details
				var max_troops = Game.armies[move_from].get_troops()
				var value_details = {
					"title": "Army Movement",
					"body": "Move how many troops?",
					"min": 1,
					"max": max_troops,
					"default": max_troops - 1,
				}
				Game.send_client_message(MESSAGE.REQUEST_VALUE, Game.active_player.player_id, 
					value_details
				)

func recv_value(details):
	match current_phase:
		ACTION.ATTACK_A, ACTION.ATTACK_B:
			var troops = details["value"]
			move_troops = troops
			move_left_troops = Game.armies[move_from].get_troops() - troops
			Game.server_prepare_move(move_from, move_to, troops)


func scoring()->bool:
	
	# 1 point per region
	for coords in Game.armies:
		var army = Game.armies[coords]
		army.player.points += 1

	# other scoring due later

	if Game.turn_count >= GameDefs.max_turns:
		print("GAME OVER")

		var winner
		var high_score = -1
		for player in Game.get_human_players():
			if player.points >= high_score:
				winner = player
				high_score = player.points

		Game.update_gui_title(ProjectSettings.get_setting('application/config/name') + " over")
		Game.update_gui_status("%s wins!" % [winner.name])

		# dance time
		for coords in Game.armies:
			var army = Game.armies[coords]
			if army.player == winner:
				army.animation_name = "dance"

		return true
	else:
		return false




