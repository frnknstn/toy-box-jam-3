extends AnimatedSprite

const ANIMATION_NAME = "walk"

func do_flight_animation(frames, from_coords:Vector2, to_coords:Vector2, time: float, animation_name:String=ANIMATION_NAME):
	self.frames = frames
	self.animation = animation_name
	self.playing = true
	self.scale = Vector2(4, 4)
	
	print("flight from %s to %s" % [from_coords, to_coords])
	
	var tween = $Tween
	tween.interpolate_property(self, "position",
		from_coords, to_coords, time, 
		tween.TRANS_QUART, tween.EASE_IN_OUT
	)
	tween.start()


func _on_Tween_tween_all_completed():
	print("flight tween over")
	self.queue_free()
