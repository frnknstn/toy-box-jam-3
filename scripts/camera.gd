extends Camera

const MAX_VERTICAL_ROTATION = PI * 9 / 20
const LOOKIT_LENGTH = 25

export var speed = 0.180

var _last_lookit = null
var hover_area: Node = null
export(NodePath) var selected_area_path
onready var selected_area : Node = get_node(selected_area_path)

func _physics_process(_delta):
	# movement processing
	var direction = Vector3.ZERO
	var velocity = Vector3.ZERO
	var speed_multiplier = 1

	# check input
	if Input.is_action_pressed("c_right"):
		direction.x += 1
	if Input.is_action_pressed("c_left"):
		direction.x -= 1
	if Input.is_action_pressed("c_backward"):
		direction.z += 1
	if Input.is_action_pressed("c_forward"):
		direction.z -= 1
	if Input.is_action_pressed("c_run"):
		speed_multiplier = 2

	if direction != Vector3.ZERO:
		direction = direction.normalized()

		velocity = direction * speed * speed_multiplier

		if velocity != Vector3.ZERO:
			# move us on a horizontal plane
			self.global_translate(velocity.rotated(Vector3.UP, rotation.y))
	
	# hex picking
	if (Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED):
		do_lookit()
	
	
func do_lookit():
	# find the nearest InteractArea under the mouse
	var mouse_pos = get_viewport().get_mouse_position()
	var from = self.project_ray_origin(mouse_pos)
	var to = from + self.project_ray_normal(mouse_pos) * LOOKIT_LENGTH

	var space_state = get_world().direct_space_state
	var result = space_state.intersect_ray(from, to, [self], 0b1000, false, true)
	if result:
		result = result["collider"]
	else:
		result = null
	
	if (_last_lookit != result):
		if (_last_lookit and not result):
			# unselect
			hover_area = null
			selected_area.set_visible(false)
		else:
			# select
			hover_area = result
			selected_area.set_visible(true)
			selected_area.transform.origin = hover_area.transform.origin
		_last_lookit = result


func _unhandled_input(event: InputEvent):
#	print(event)
	# for debug, rotate our camera if RMB is pressed
	if event is InputEventMouseButton:
		match event.button_index:
			BUTTON_RIGHT: 
				# Only allows rotation if right click down
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED if event.pressed else Input.MOUSE_MODE_VISIBLE)
			BUTTON_LEFT:
				# do a thing
				if event.pressed:
					Game.click_area(hover_area)

	elif event is InputEventMouseMotion:
		if (Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED):
#			get_tree().set_input_as_handled()
			var rotation_acc := Vector3.ZERO
			rotation_acc.x -= (event.relative.x * GameDefs.mouse_sensitivity)
			rotation_acc.y += (event.relative.y * GameDefs.mouse_sensitivity * GameDefs.mouse_uninvert_factor)

			# lock our look up / down
			rotation_acc.y = clamp(
				rotation_acc.y, 
				-MAX_VERTICAL_ROTATION - self.rotation.x, 
				MAX_VERTICAL_ROTATION - self.rotation.x
			)

			self.rotate_y(rotation_acc.x) # first rotate around the Y
			self.rotate_object_local(Vector3.RIGHT, rotation_acc.y) # then rotate around the X (waist)
