extends CanvasLayer

export(NodePath) var title_label_path
export(NodePath) var status_label_path
export(NodePath) var input_window_path
export(NodePath) var assignment_window_path


signal return_value
signal return_assignment

var supply_display = []
var supply_sprite = preload("res://scenes/gui_supplysprite.tscn")

onready var supply_count_label = $SupplyChain/SupplyCount
onready var supply_icons_node = $SupplyChain/SupplyIcons

var _status_before_hint: String = ""
var _status_before_sound: String = ""

func _ready():
	if Game.connect("update_status", self, "_on_update_status") != OK:
		assert(false)
	if Game.connect("update_title", self, "_on_update_title") != OK:
		assert(false)
	if Game.connect("request_value", self, "_on_request_value") != OK:
		assert(false)
	if Game.connect("request_assignment", self, "_on_request_assignment") != OK:
		assert(false)

func _process(_delta):
	"hack to get a point display in before submission"
	var label = $Points/Label/Label
	var s = ""
	var player

	player = Game.players[0]
	s = s +("%s: %dGP %dVP / " % [player.name, player.money, player.points])
	player = Game.players[1]
	s = s +("%s: %dGP %dVP\n" % [player.name, player.money, player.points])
	player = Game.players[2]
	s = s +("%s: %dGP %dVP / " % [player.name, player.money, player.points])
	player = Game.players[3]
	s = s +("%s: %dGP %dVP\n" % [player.name, player.money, player.points])
	s = s + "turn %d/%d" % [Game.turn_count, GameDefs.max_turns]
	label.text = s

func _on_update_status(s: String):
	var label = get_node(status_label_path)
	if label.text != s:
		print("Update status to '%s'" % [s])
		label.text = s
		if s and (s.left(7).to_lower() != 'waiting') and (s != _status_before_sound):
			$SoundClick2.play()
			_status_before_sound = s
	_status_before_hint = s

func _on_update_title(s: String):
	var label = get_node(title_label_path)
	if label.text != s:
		print("Update title to '%s'" % [s])
		label.text = s
		$SoundClick1.play()

func _on_option_hint(s: String):
	var label = get_node(status_label_path)
	var hint = _status_before_hint + " - " + s
	if label.text != hint:
		label.text = _status_before_hint + " - " + s
		$SoundClick2.play()

func _on_request_value(d: Dictionary):
	var box = get_node(input_window_path)
	box.show_window(true, d["title"], d["body"], d["min"], d["max"], d.get("default", d["max"]))

func _on_request_assignment(title: String, d: Array):
	var box = get_node(assignment_window_path)
	box.show_window(true, title, d)

func assign_to_selected(coords: Array):
	"""The game is telling us to assign the coords to the current option

	Returns the node that accepted the assignment, or null if assignment fails
	"""
	var box = get_node(assignment_window_path)
	return box.assign_to_selected(coords)


func _on_supplies_changed(supplies: Array):
	# let's just go the things the easy way
	supply_count_label.text = "%2d" % len(supplies)
	
	# clear the old display
	for spr in supply_display:
		spr.queue_free()
	supply_display.clear()
	
	# Create the new supply
	var y := 0
	supplies.sort()
	for player in supplies:
		var spr = supply_sprite.instance()
		spr.frames = player.frames
		spr.transform.origin.y = y
		supply_icons_node.add_child(spr)
		supply_display.append(spr)
		y += 27
		

func _on_InputGo_pressed():
	var box = get_node(input_window_path)
	emit_signal("return_value", box.get_value())
	box.show_window(false)


func _on_AssignmentGo_pressed():
	var box = get_node(assignment_window_path)
	var assignment = box.get_assignment()
	box.show_window(false)
	emit_signal("return_assignment", assignment)

