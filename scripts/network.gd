extends Reference
class_name Network

const STATE = GameDefs.State
const MESSAGE = GameDefs.Message

static func send_client_message(_client_id, server_state, message_type, active_player_id, details: Dictionary):
	# stub for sending messages to clients
	print("Client message <--> %d:%s %d:%s %d %s" % [
		server_state, STATE.keys()[server_state], 
		message_type, MESSAGE.keys()[message_type], 
		active_player_id, str(details)
	])
	
	# now we handwave the next step and pretend we just got the message
	Game.client_state_details = ClientState.new(Game.client_state, active_player_id, details)
	Game.server_state = server_state

	if active_player_id != 0:
		Game.client_active_player = Game.get_player_by_id(active_player_id)
	else:
		# loop through all players
		Game.client_queued_players = []
		for player in Game.players:
			if player.is_local and not player.is_neutral:
				Game.client_queued_players.append(player)

	Game.update_gui_title()

	match message_type:
		MESSAGE.START:
			Game.client_start()
		MESSAGE.REQUEST_HEX:
			Game.client_select_hex()
		MESSAGE.REQUEST_ASSIGNMENT:
			Game.client_assignment()
		MESSAGE.MONEY:
			Game.client_money()
		MESSAGE.DEPLOY:
			Game.client_deploy()
		MESSAGE.REQUEST_VALUE:
			Game.client_select_value()
		MESSAGE.MOVE:
			Game.client_move_army()
		MESSAGE.MERGE:
			Game.client_merge_army()
		MESSAGE.FIGHT_RESULT:
			Game.client_enact_fight()

static func send_server_message(message_type:int, details):
	# stub for sending replies to the server
	print("Server message <--> %d:%s %s" % [message_type, MESSAGE.keys()[message_type], str(details)])
	
	# now we handwave the next step and pretend we just got the message

	# new-style returns
	if Game.state_helper != null:
		Game.state_helper._recv(message_type, details, Game.client_active_player)
		return

	# original returns
	match message_type:
		MESSAGE.SUBMIT_HEX:
			Game.server_submit_hex(details)
		MESSAGE.SUBMIT_VALUE:
			Game.server_submit_value(details)
		_:
			print_debug("UNHANDLED SERVER MESSAGE TYPE %d %s: %s" % [
				message_type, MESSAGE.keys()[message_type], str(details)
			])

