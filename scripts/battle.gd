extends Reference

enum EncounterResult {
	NO_ENCOUNTER,
	LOST,
	ADVENTURE
}

class Side:
	var player
	var troops: int
	var supplies: int # converted to troops at the start of combat, and any excess back to supply at the end
	var original_troops: int
	func _init(p_player, p_troops, p_supplies):
		self.player = p_player
		self.troops = p_troops + p_supplies 
		self.supplies = 0
		self.original_troops = p_troops

class Troop:
	var side: Side
	var enemy: Side
	func _init(p_side: Side, p_enemy: Side):
		self.side = p_side
		self.enemy = p_enemy
		


var a_side
var b_side
var chain
var add_chain: Array
var remove_chain: Array
var rounds : int = 0

func _init(attacker, defender, a_army: Army, b_army: Army, p_chain):
	self.a_side = Side.new(attacker, a_army.get_troops(), a_army.get_supply())
	self.b_side = Side.new(defender, b_army.get_troops(), b_army.get_supply())
	
	self.chain = p_chain.to_array()	# a list of the supply in the chain, represented by the Player objects
	self.add_chain = []
	self.remove_chain = []


func do_fight():
	"do a complete 4-round fight"
	do_round()
	do_round()
	do_round()
	do_round()
	return resolve()

func do_round():
	"do a round of combat"
	rounds += 1
	
	# get all the troops in a random order and run them through the encounter
	var troop_list: Array = []
	var troop_proto: Troop
	
	troop_proto = Troop.new(a_side, b_side)
	for _i in range(a_side.troops):
		troop_list.append(troop_proto)
	troop_proto = Troop.new(b_side, a_side)
	for _i in range(b_side.troops):
		troop_list.append(troop_proto)	
	
	troop_list.shuffle()
	
	# do the encounters
	for troop in troop_list:
		var result = encounter(troop)
		troop.side.troops += result[0]
		troop.enemy.troops += result[1]
		
		if result[2]:
			add_chain.append(troop.side.player)
		if result[3]:
			add_chain.append(troop.enemy.player)
	

func encounter(troop: Troop)-> Array:
	"""Send a troop from a side on an encounter
	
	Returns an array of results = [
		change to friendly troop count: int,
		change to enemy troop count: int,
		add a friendly supply: bool,
		add an enemy supply: bool
	]
	"""
	
	var result := [0, 0, 0, 0]
	var hero = troop.side.player
	
	while hero != null:
		match _encounter_result(len(chain)):
			EncounterResult.NO_ENCOUNTER:
				hero = null
			EncounterResult.LOST:
				# hero was lost, convert it to supply
				if hero == troop.side.player:
					result[0] -= 1
					result[2] += 1
				elif hero == troop.enemy.player:
					result[1] -= 1
					result[3] += 1
				hero = null
			EncounterResult.ADVENTURE:
				# bump into a random troop, and that troop does a single encounter
				hero = chain[randi() % len(chain)]
				if hero == troop.side.player:
					# found a friend in the chain
					result[0] += 1
					chain.erase(hero)
					remove_chain.append(hero)
				if hero == troop.enemy.player:
					# found an enemy
					result[1] += 1
					chain.erase(hero)
					remove_chain.append(hero)
				else:
					# 3rd party, don't alter the supply in the chain
					pass
	return result

func _encounter_result(total_supplies: int):
	"""Inner function to determine what happens in a single encounter."""
	# each troop has a 1/3 chance to just do nothing each round
	if randi() % 3 == 0:
		return EncounterResult.NO_ENCOUNTER
	
	# each troop has a sliding chance to either encounter supplies, or get lost and turned into supplies themselves
	# the chances depend on how much supply is in the tower
	#   2 or less supply is 1/3 encounter, 1/4 lost
	#   12 supply+ is 2/3 encounter, 1/6 lost
	var population: float = min((total_supplies - 2) / 10.0, 1.0)
	var lost_chance: float = 1.0 / (4.0 + (3.0 * population))
	var encounter_chance: float = (1.0/3.0) + ((1.0/3.0) * population) 
	
	var roll: float = randf()
	
	if roll <= lost_chance:
		return EncounterResult.LOST
	elif (roll <= lost_chance + encounter_chance) && (total_supplies > 0):
		# adventure!
		return EncounterResult.ADVENTURE
	else:
		return EncounterResult.NO_ENCOUNTER

func resolve():
	"end the fight"
	# calculate casualties, 1:1
	var a_total: int = a_side.troops
	var b_total: int = b_side.troops
	var lost = min(a_total, b_total)
	
	var a_survived = a_total - lost
	var b_survived = b_total - lost
	a_side.troops = a_survived
	a_side.supplies = 0
	b_side.troops = b_survived
	b_side.supplies = 0
	
	# convert excess troops to supply
	if a_survived >= a_side.original_troops:
		a_side.supplies = a_survived - a_side.original_troops
		a_side.troops = a_side.original_troops
	if b_survived >= b_side.original_troops:
		b_side.supplies = b_survived - b_side.original_troops
		b_side.troops = b_side.original_troops
	
	# add new supply to the chain?
	
	return [a_side.troops, a_side.supplies, b_side.troops, b_side.supplies, add_chain, remove_chain]

