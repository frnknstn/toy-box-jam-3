extends Node

const STATE = GameDefs.State
const MESSAGE = GameDefs.Message
const HEX_SPEC = GameDefs.HexSpec

var submissions = {}
var awaiting: Array
var options

func _start():
	"called when entering a state"
	awaiting = Game.get_human_players()

	var options = []
	for i in range(len(GameDefs.start_armies)):
		var val = GameDefs.start_armies[i]
		if val <= 2:
			# we will make everything size 2 by default
			continue

		options.append(
				# [option_id, icon, text (6 chars), hint text]
				[i, load("res://sprites/icon-%d.tres" % [val]), "troops", "army with %d troops" % [val]]
			)

	Game.send_client_message(MESSAGE.REQUEST_ASSIGNMENT, 0, {
		"hex_spec": HEX_SPEC.FRIENDLY_ARMY,
		"options": options,
	})
	

func _recv(message_type: int, details: Dictionary, player=null):
	"called when we get a message"

	match message_type:
		MESSAGE.SUBMIT_ASSIGNMENT:
			print("Player %s has submitted" % [player.name])
			awaiting.erase(player)
			submissions[player] = details

			if len(awaiting) == 0:
				# we are done
				parse_submissions()
				# next phase
				Game.call_deferred("change_game_state", STATE.NEW_ROUND)

		_:
			print("UNHANDLED SERVER MESSAGE %d %s" % [ message_type, MESSAGE.keys()[message_type] ])
			assert(false)


func parse_submissions():
	for player in submissions:
		var details = submissions[player]["value"]
		# the details are in the form {option_id: coords, ...}

		# grow the specified army
		for option_id in details:
			var coords = details[option_id]
			if coords == null:
				# option skipped
				continue
			var army = Game.armies[coords]
			var size = GameDefs.start_armies[option_id]
			army.resize(size, 0)

	# everything else becomes a 2-size army
	for army in Game.armies.values():
		if army.get_troops() < 2:
			army.resize(2, 0)










