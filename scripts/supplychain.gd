extends Reference

# Not sure exactly how to best do this, so simply a list of Players, with each entry = 1 supply

signal change_supplies	# (Array: Player)

var _supplies = []

func add_supplies(player):
	_supplies.append(player)
	emit_signal("change_supplies", to_array())

func remove_supplies(player):
	_supplies.erase(player)
	emit_signal("change_supplies", to_array())

func set_supplies(chain):
	_supplies.clear()
	_supplies.append_array(chain)
	emit_signal("change_supplies", to_array())

func to_array():
	return _supplies.duplicate()

func size():
	return _supplies.size()
