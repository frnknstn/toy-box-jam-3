extends Node2D

export(Resource) var texture setget _set_texture

var origin
var viewport

func _set_texture(p_texture):
	texture = p_texture
	$Sprite.texture = p_texture
	$Shadow.texture = p_texture

func _process(_delta):
	position = viewport.get_camera().unproject_position(origin)

func _die1(_arg):
	"signal death helper with one arg"
	queue_free()
