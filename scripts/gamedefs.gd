extends Node

const SETTINGS_PATH = "user://settings.tres"

const BUTTON_LEFT = 1
const BUTTON_RIGHT = 2

var battle_animation_delay = 0.667

var mouse_uninvert_factor = 1
var mouse_sensitivity = 0.0058

# settings direct variables
var uninvert_mouse_y = true setget set_uninvert_mouse_y
var sound_mute = false setget set_sound_mute
var fight_animations = true
var auto_pick_land = true


# game settings
const max_turns = 6
const start_armies = [5, 4, 4, 3, 3, 2, 2, 2]

enum Action {
	MONEY,
	BUILD,
	DEPLOY_5,
	DEPLOY_3,
	DEPLOY_MOVE_1,
	ATTACK_A,
	ATTACK_B,
	RESUPPLY,
}

var action_options = [
	[Action.MONEY,load("res://sprites/icon-money.tres"), "MONEY", "gain 6 money"],
#	[Action.BUILD, load("res://sprites/icon-6.tres"), "", ""],
	[Action.DEPLOY_5, load("res://sprites/icon-5.tres"), "TROOPS", "deploy 5 troops for 3 money"],
	[Action.DEPLOY_3, load("res://sprites/icon-3.tres"), "TROOPS", "deploy 3 troops for 2 money"],
#	[Action.DEPLOY_MOVE_1, load("res://sprites/icon-1.tres"), "TROOP^", "deploy 1 troop for 1 money, and then move any number to adjacent army"],
	[Action.ATTACK_A,load("res://sprites/icon-attack1.tres"), "ATTACK", "attack or move from this area"],
	[Action.ATTACK_B,load("res://sprites/icon-attack2.tres"), "ATTACK", "attack or move from this area"],
#	[Action.RESUPPLY,load("res://sprites/icon-6.tres"), "", ""],
]

# things a player may be required to do
enum State {
	NOTHING,
	WAITING,
	START,

	# <active dev>
	OFFER_LAND,
	PLACE_ARMY,
	SELECT_ASSIGNMENT,
	NEW_ROUND,
	NEW_PHASE,
	MOVE,
	MERGE,
	FIGHT,
	# </active dev>

	SELECT_HEX,
	SELECT_NEIGHBOR,
	SELECT_VALUE,
	PREMOVE,
	END_PHASE,
	GAME_OVER,
}

enum Message {
	START,

	# <active dev>
	REQUEST_HEX,
	SUBMIT_HEX,
	REQUEST_ASSIGNMENT,
	SUBMIT_ASSIGNMENT,
	MONEY,
	DEPLOY,
	REQUEST_VALUE,
	SUBMIT_VALUE,
	# </active dev>

	MOVE,
	MERGE,
	FIGHT_RESULT,
	MOVE_COMPLETE,	# move action and its associated animations is complete
	FIGHT_COMPLETE,	# fight action and its associated animations is complete
	# </semiactive dev>

	END_PHASE,
	READY_FOR_NEXT_PHASE,
}

# the details of a hex request
enum HexSpec {
	ANY,
	FRIENDLY_ARMY,
	NEIGHBOR,
}


var SettingsResource = load("res://resources/settingsresource.gd")


func load_settings():
	print("Loading settings")
	var settings = ResourceLoader.load(SETTINGS_PATH, "", true)
	if not settings:
		print("Creating new settings resource")
		settings = SettingsResource.new()
		save_settings()
	else:
		apply_settings(settings)
	return settings

func apply_settings(res):
	"apply the settings in the supplied SettingsResource"
	GameDefs.uninvert_mouse_y = res.uninvert_mouse_y
	GameDefs.sound_mute = res.sound_mute
	GameDefs.fight_animations = res.fight_animations

func save_settings():
	var res = SettingsResource.new()
	res.uninvert_mouse_y = GameDefs.uninvert_mouse_y
	res.sound_mute = GameDefs.sound_mute
	res.fight_animations = GameDefs.fight_animations
	
	if ResourceSaver.save(SETTINGS_PATH, res) != OK:
		print("Error saving settings")
	else:
		print("Settings saved")

func set_uninvert_mouse_y(b: bool):
	uninvert_mouse_y = b
	GameDefs.mouse_uninvert_factor = 1 if b else -1

func set_sound_mute(b: bool):
	print("sound mute: ", b)
	sound_mute = b
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), b)
