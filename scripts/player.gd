extends Reference

var player_id: int
var name: String

var is_neutral: bool = false
var is_busy: bool = false
var is_local: bool = true

var frames: SpriteFrames

var start_hexes = []
var action_plan: Dictionary		# the player's submitted action assignment

var money: int = 0
var points: int = 0

func _init(p_player_id:int, p_name: String, p_frames: SpriteFrames, p_money):
	player_id = p_player_id
	name = p_name
	frames = p_frames
	money = p_money
