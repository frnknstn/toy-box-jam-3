extends Spatial
class_name HexGrid

var InteractArea = preload("res://scenes/interactarea.tscn")

const ROOT_THREE = HexMap.ROOT_THREE
const HEX_WD = HexMap.HEX_WD
const HEX_WR = HexMap.HEX_WR

const RADIUS: float = 1.0
const HEIGHT: float = 2.0
const Y_STEP: float = 0.25
const Y_BASE: float = 1.51
const Z_STEP: float = HexMap.Z_STEP

var hexmap: HexMap
onready var areas := $Areas

var _highlights = []
var _highlight_scene = preload("res://scenes/ringoffire.tscn")


const HEX_VERTS = [
	Vector3(0.0, 0.0, 0.0), 		# 0
	Vector3(0.0, 0.0, -1.0),
	Vector3(HEX_WR, 0.0, -0.5), 
	Vector3(HEX_WR, 0.0, 0.5), 		# 3
	Vector3(0.0, 0.0, 1.0), 
	Vector3(-HEX_WR, 0.0, 0.5), 
	Vector3(-HEX_WR, 0.0, -0.5), 	# 6
	
	Vector3(0.0, -HEIGHT, -1.0),		# 7 (bottom)
	Vector3(HEX_WR, -HEIGHT, -0.5), 
	Vector3(HEX_WR, -HEIGHT, 0.5), 	# 9
	Vector3(0.0, -HEIGHT, 1.0), 
	Vector3(-HEX_WR, -HEIGHT, 0.5), 
	Vector3(-HEX_WR, -HEIGHT, -0.5), 	# 12
]

# the X are technically supposed to be from (1-HEX_WR)/2 to (1+HEX_WR)/2
# ~~ (0.0669872981077807 to 0.9330127018922193)
const HEX_TOP_UVS = [
	Vector2(0.5, 0.5),		# 0
	Vector2(0.5, 1.0),
	Vector2(1.0, 0.75),
	Vector2(1.0, 0.25),		# 3
	Vector2(0.5, 0.0),
	Vector2(0.0, 0.25),
	Vector2(0.0, 0.75),		#6
]

# UVs to use on the sides of the prism
const HEX_SIDE_UV_TOP_LEFT = [
	Vector2(0.0, 0.0),	# TL
	Vector2(1.0, 0.0),	# TR
	Vector2(0.0, HEIGHT),	# BL
]
const HEX_SIDE_UV_BOTTOM_RIGHT = [
	Vector2(1.0, 0.0),	# TR
	Vector2(1.0, HEIGHT),	# BR
	Vector2(0.0, HEIGHT),	# BL
]

const HEX_TOP_TRIS = [[1,2,6], [6,2,5], [2,3,5], [5,3,4]]
const HEX_SIDE_TRIS = [
	# prism faces in clockwise order looking down
	[], 	# top side, ignore
	[[2,1,8], [1,7,8]],		# "hidden" face 1
	[[3,2,9], [2,8,9]],
	[[4,3,10], [3,9,10]],
	[[5,4,11], [4,10,11]],
	[[6,5,12], [5,11,12]],
	[[1,6,7], [6,12,7]],	# "hidden" face 6
]

func _ready():
	
	var top_mat_base = preload("res://materials/hex_top.tres")
	var side_mat_base = preload("res://materials/hex_side.tres")
	
	hexmap = HexMap.new()
	hexmap.load_resource("res://resources/map.tres")
	
	var surface_buckets = group_map_surfaces(hexmap.map_data)
	
	var top_mesh = null
	var side_mesh = null
	var meshinstance = null
	
	var top_count := 0
	var side_count := 0
	
	# DEBUG DEMO
	
	for material_name in surface_buckets:
		print("material %s has %d sides" % [material_name, len(surface_buckets[material_name])])
		# create the materials
		var top_mat = top_mat_base.duplicate()
		var side_mat = side_mat_base.duplicate()
		var texture_material = MaterialMan.texture_lookup[material_name]
		top_mat.albedo_texture = texture_material
		side_mat.albedo_texture = texture_material
		
		var st_top = SurfaceTool.new()
		var st_side = SurfaceTool.new()
		st_top.begin(Mesh.PRIMITIVE_TRIANGLES)
		st_side.begin(Mesh.PRIMITIVE_TRIANGLES)
		st_top.set_material(top_mat)
		st_side.set_material(side_mat)
		
		for side in surface_buckets[material_name]:
			var coords = side.slice(1, 3)
			var xyz = grid3_to_world(coords)
			
			# make the surfaces
			if side[0] == 0:
				# this is on the of the prism
				make_top(xyz, st_top)
				make_interact_area(xyz, [coords[0],coords[2]])
				top_count += 1
			
			else:
				# this is on the side of the prism
				
				var neighbor_coords = hexmap.hex_neighbor_coords([coords[0], coords[2]])[side[0]-1]
				var neighbor = hexmap.get_hex_at(neighbor_coords)
				if (neighbor == null):
					# draw a full-height side
					# TODO uniform floor level?
					# TODO shorten the sides that are not obscured, but closer to the ground
					pass
				elif (neighbor.elevation >= coords[1]):
					# neighbor is bigger than us, skip this side
					continue
				else:
					# TODO shorten the sides that are partially obscured
					#hexmap.coord_to_pos(coords)
					pass
				
				# make the tris
				make_side(xyz, st_side, side[0])
				side_count += 1
		
		st_top.generate_normals()
		#st_top.generate_tangents()
		top_mesh = st_top.commit(top_mesh)
		
		st_side.generate_normals()
		#st_side.generate_tangents()
		side_mesh = st_side.commit(side_mesh)
		
		# add the meshes to the tree
		meshinstance = MeshInstance.new()
		meshinstance.set_mesh(top_mesh)
		$Geometry.add_child(meshinstance)
		
		meshinstance = MeshInstance.new()
		meshinstance.set_mesh(side_mesh)
		$Geometry.add_child(meshinstance)
		
	print("ia_count ", ia_count)
	
	print("Hex grid created, %d tops and %d sides" % [top_count, side_count])
	
	
func make_top(xyz, st):
	for tri in HEX_TOP_TRIS:
		st.add_uv(HEX_TOP_UVS[tri[0]])
		st.add_vertex(HEX_VERTS[tri[0]] + xyz)
		st.add_uv(HEX_TOP_UVS[tri[1]])
		st.add_vertex(HEX_VERTS[tri[1]] + xyz)
		st.add_uv(HEX_TOP_UVS[tri[2]])
		st.add_vertex(HEX_VERTS[tri[2]] + xyz)
		
	
func make_side(xyz, st, side_num):
	var tris = HEX_SIDE_TRIS[side_num]
	
	# top left triangle
	st.add_uv(HEX_SIDE_UV_TOP_LEFT[0])
	st.add_vertex(HEX_VERTS[tris[0][0]] + xyz)
	st.add_uv(HEX_SIDE_UV_TOP_LEFT[1])
	st.add_vertex(HEX_VERTS[tris[0][1]] + xyz)
	st.add_uv(HEX_SIDE_UV_TOP_LEFT[2])
	st.add_vertex(HEX_VERTS[tris[0][2]] + xyz)
	
	# bottom right triangle
	st.add_uv(HEX_SIDE_UV_BOTTOM_RIGHT[0])
	st.add_vertex(HEX_VERTS[tris[1][0]] + xyz)
	st.add_uv(HEX_SIDE_UV_BOTTOM_RIGHT[1])
	st.add_vertex(HEX_VERTS[tris[1][1]] + xyz)
	st.add_uv(HEX_SIDE_UV_BOTTOM_RIGHT[2])
	st.add_vertex(HEX_VERTS[tris[1][2]] + xyz)


func group_map_surfaces(map_data):
	"convert a 2D map data structure into lists of surfaces and grid locations, grouped by material types"
	
	var retval = {}
	
	var z: int = 0
	for row in map_data:
		var x: int = 0
		for item in row:
			var y = item["elevation"]
			var sides = item["sides"]
			
			# add the sides
			for i in range(7):
				var bucket_name = sides[i]
				var bucket = retval.get(bucket_name, [])
				bucket.append([i, x, y, z])
				retval[bucket_name] = bucket
			
			x += 1
		z += 1
	
	return retval

func grid3_to_world(coords):
	"convert a hex offset coord triple into a Vector3 of actual engine coords"
	var x_base := 0.0
	if coords[2] % 2 != 0:
		x_base = HEX_WR
	
	return Vector3(
		x_base + (HEX_WD * coords[0]), 
		Y_BASE + (Y_STEP * coords[1]), 
		Z_STEP * coords[2]
	)
	

func grid2_to_world(coords):
	"convert a hex offset coord [x,z] into a Vector3 of actual engine coords"
	# fetch the elevation from the map data
	return grid3_to_world([coords[0], hexmap.get_hex_at(coords)["elevation"], coords[1]])

var ia_count := 0

func make_interact_area(xyz, coords):
	"construct an interactable point on the top of all the hexes"
	var ia = InteractArea.instance()
	ia.coords = coords
	ia.transform.origin = xyz
	$Areas.add_child(ia)
	ia_count += 1
	
func show_sprite(node, coords):
	"Situate and display this sprite-based node in our tree"
	node.transform.origin = grid2_to_world(coords)
	$SpriteLayer.add_child(node)

func move_sprite(node, coords):
	"move a sprite to the correct world coords"
	node.move_animation(grid2_to_world(coords))

func clear_highlight():
	"remove hex highlights"
	print("clear highlights")
	for hh in _highlights:
		hh.queue_free()
	_highlights.clear()
	
func highlight_hex(coords):
	"add a highlight to one hex"
	var hh = _highlight_scene.instance()
	hh.transform.origin = grid2_to_world(coords)
	self.add_child(hh)
	_highlights.append(hh)

func highlight_adjacent(coords):
	"add a highlight to the hexes reachable from the supplied coords"
	for hex in hexmap.hex_connected_neighbors(coords):
		highlight_hex(hex)

func highlight_player_armies(player, armies: Dictionary):
	"add a highlight to all the hexes with a player's troops"
	for army_hex in armies:
		var army = armies[army_hex]
		if army.get_troops() > 0:
			if army.player == player:
				highlight_hex(army_hex)
