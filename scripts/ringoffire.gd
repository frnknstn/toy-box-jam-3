extends Spatial

func _ready():
	# randomise our flames
	for j in range(1, 7):
		var side_name = "side_%d" % [j]
		for i in range(1, 6):
			var spr_name = "flame_%d" % [i]
			var node = get_node(side_name + "/" + spr_name)

			# start animation on a random frame
			node.frame = randi() % node.frames.get_frame_count("default")


