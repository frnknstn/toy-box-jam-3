extends Node

"This state is for requesting actions from players"

const STATE = GameDefs.State
const MESSAGE = GameDefs.Message
const HEX_SPEC = GameDefs.HexSpec

var submissions = {}
var awaiting: Array
var options

func _start():
	"called when entering a state"
	Game.turn_count += 1
	
	awaiting = Game.get_human_players()

	var options = GameDefs.action_options.duplicate()

	Game.send_client_message(MESSAGE.REQUEST_ASSIGNMENT, 0, {
		"hex_spec": HEX_SPEC.FRIENDLY_ARMY,
		"options": options,
		"title": "plans",
	})
	

func _recv(message_type: int, details: Dictionary, player=null):
	"called when we get a message"

	match message_type:
		MESSAGE.SUBMIT_ASSIGNMENT:
			print("Player %s has submitted" % [player.name])
			awaiting.erase(player)
			submissions[player] = details

			if len(awaiting) == 0:
				# we are done
				parse_submissions()

		_:
			print("UNHANDLED SERVER MESSAGE %d %s" % [ message_type, MESSAGE.keys()[message_type] ])
			assert(false)


func parse_submissions():
	for player in submissions:
		var details: Dictionary = submissions[player]["value"]

		# the details are in the form {option_id: coords, ...}
		player.action_plan = details

	# next phase
	Game.call_deferred("change_game_state", STATE.NEW_PHASE)









