extends Node

export(NodePath) var environment_path

var title = "Toy Box Jam 3 v0.0.1"

var FontMaker = preload("res://fonts/fontmaker.gd")

func _ready():
	resize()
	randomize()

	# tweak for web
	if OS.has_feature("web"):
		var env = get_node(environment_path)
		env.environment = load("res://web_env.tres")

	# FontMaker.make()
	GameDefs.load_settings()
	
	Game.start_game(
		get_node("ViewportContainer/Viewport/Spatial/HexGrid"),
		get_node("GUI")
	)


func _process(_delta):
	OS.set_window_title(title + " | fps: " + str(Engine.get_frames_per_second()))
	
	#get_viewport().rect.size = OS.get_windows_size()
	resize()
	

func resize():
	$ViewportContainer.get_rect().size = OS.get_window_size()
	$ViewportContainer/Viewport.size = OS.get_window_size()

