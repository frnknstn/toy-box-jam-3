extends Control

export(NodePath) var input_value_path
export(NodePath) var display_value_path
export(NodePath) var input_go_button_path
export(NodePath) var title_label_path
export(NodePath) var body_label_path


func _ready():
	show_window(false)

func show_window(show: bool, title: String="", body: String="", p_min: int=0, p_max: int=6, p_default: int=6):
	if show:
		var input = get_node(input_value_path)
		input.min_value = p_min
		input.max_value = p_max
		input.value = p_default
		get_node(input_go_button_path).grab_focus()

		get_node(title_label_path).text = title
		get_node(body_label_path).text = body

	visible = show

func get_value()->int:
	return get_node(input_value_path).value


func _on_InputValue_value_changed(value: int):
	get_node(display_value_path).text = " %d " % [value]

