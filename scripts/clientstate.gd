extends Reference
class_name ClientState

"Client state helper object"

var state: int
var active_player_id: int
var details: Dictionary


func _init(p_state:int, p_active_player_id: int, p_details: Dictionary):
	assert(p_state in GameDefs.State.values())
	self.state = p_state
	self.active_player_id = p_active_player_id
	self.details = p_details

