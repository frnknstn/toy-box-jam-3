tool

class_name ShadeSprite

extends Spatial

export(SpriteFrames) var frames: SpriteFrames = preload("res://sprites/tbj2_all.tres") setget set_spriteframes
export(String) var animation_name: String = "stand" setget set_animation_name


func set_spriteframes(new: SpriteFrames):
	$FaceSprite.frames = new
	$ShadowSprite.frames = new
	frames = new
	
func set_animation_name(new: String):
	animation_name = new
	$FaceSprite.animation = new
	$ShadowSprite.animation = new
	
