extends Spatial
class_name Army

tool

const FlyingSprite = preload("res://scenes/flyingsprite.tscn")

const DEFAULT_ANIMATION_NAME: String = "stand"
const MAX_SPRITES: int = 9
const MOVE_ANIM_TIME: float = 0.82
const SUPPLIES_FRAMES = preload("res://sprites/supplies.tres")
const SUPPLIES_ANIMATION_NAME = "bag"
const FIRE_SPRITES = preload("res://sprites/fire.tres")
const FIRE_ANIMATION_NAME = "once"
const POOF_SPRITES = preload("res://sprites/poof.tres")
const POOF_ANIMATION_NAME = "once"

export(SpriteFrames) var frames: SpriteFrames = preload("res://sprites/tbj2_all.tres") setget set_spriteframes
export(String) var animation_name: String = DEFAULT_ANIMATION_NAME setget set_animation_name


var player

var _supply_spriteframes: SpriteFrames = SUPPLIES_FRAMES
var _supply_animation_name := SUPPLIES_ANIMATION_NAME

var _troops: int = 0
var _supply: int = 0

var _sprites := []
var _total_size: int = 0

var _spr_template = preload("res://scenes/shadesprite.tscn")

func get_troops()->int:
	return _troops

func get_supply()->int:
	return _supply

func get_total()->int:
	return _troops + _supply

func resize(p_troops: int, p_supply: int):
	_troops = p_troops
	_supply = p_supply
	_total_size = _troops + _supply
	_resize()


func _resize():
	"add or remove sprites to match our numbers"
	
	# work out exactly what we want to display
	var target_troops = 0
	var target_supply = 0
	var target_total = 0
	
	if _total_size > MAX_SPRITES:
		# favor showing troops over supply
		target_troops = min(_troops, MAX_SPRITES)
		target_supply = MAX_SPRITES - target_troops
	else:
		# we can show all
		target_troops = _troops
		target_supply = _supply
		
	target_total = target_troops + target_supply
	
	# are we dead:
	if target_total == 0:
		print("We have no stuff, destroying... %d %d %d" %[target_troops, target_supply, target_total])
		destroy()
		return
	
	# remove sprites if needed
	while target_total < len(_sprites):
		var sprite = _sprites.pop_back()
		sprite.queue_free()
	
	# add sprites if needed:
	while target_total > len(_sprites):
		var sprite = _spr_template.instance()
		_sprites.append(sprite)
		add_child(sprite)
	
	# change the sprites to match requirements
	for i in range(target_total):
		var sprite = _sprites[i]
		
		# set skin / animation
		if i < target_troops:
			# it's a troop
			sprite.frames = frames
			sprite.animation_name = animation_name
		else:
			# it's supply
			sprite.frames = _supply_spriteframes
			sprite.animation_name = _supply_animation_name
		
		# change position
		var position_name = "size_%d/slot_%d" % [target_total, i+1]
		_move_sprite(sprite, position_name)
		

func _move_sprite(sprite: ShadeSprite, position_name: String):
	"move a sprite to a named position within the army"
	# TODO: funky animation
	sprite.transform.origin = get_node(position_name).transform.origin

func destroy():
	print("Removing army")
	self.queue_free()

func set_spriteframes(new: SpriteFrames):
	"Change our troops to match new frames"
	frames = new
	for i in range(_troops):
		_sprites[i].frames = new
	
func set_animation_name(new: String):
	"Change our troops to match new animation"
	if not Engine.editor_hint:
		print("Army animation set to '%s'" % [new])
	animation_name = new
	for spr in _sprites:
		if spr.frames != SUPPLIES_FRAMES:
			spr.animation_name = new

func move_animation(to):
	if self.animation_name != "dance":
		set_animation_name("walk")
		frames.set_animation_speed("walk", 8.0)
	$Tween.interpolate_property(self, "translation", null, to, MOVE_ANIM_TIME, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()

func _on_Tween_tween_all_completed():
	# go back to default stance
	if self.animation_name != "dance":
		set_animation_name(DEFAULT_ANIMATION_NAME)

func animate_fight(b_army, results, chain):
	"Coroutine to animate a battle"

	# objectives:
	# make the fight seem meaningful
	# make the fight seem fun
	# show changes to the supply chain

	var a_army = self


	# apply the supply change to our armies
	var add_supplies = results[4]
	var remove_supplies = results[5]
	var a_add_supplies := 0
	var b_add_supplies := 0
	for s in add_supplies:
		if s == a_army.player:
			a_add_supplies += 1
		elif s == b_army.player:
			b_add_supplies += 1

	for s in remove_supplies:
		if s == a_army.player:
			a_add_supplies -= 1
		elif s == b_army.player:
			b_add_supplies -= 1

	# animate the supply effects
	if a_add_supplies > 0:
		# some troops became supplies
		for _i in range(a_add_supplies):
			_fly_from_army(a_army)
			a_army.get_node("SoundToSupply").play()
			yield(get_tree().create_timer(GameDefs.battle_animation_delay), "timeout")
			chain.add_supplies(a_army.player)
			if a_army.get_supply() > 0:
				a_army.resize(a_army.get_troops(), a_army.get_supply() - 1)
			else:
				a_army.resize(a_army.get_troops() - 1, 0)
	else:
		# some supplies taken from the supply chain
		for _i in range(abs(a_add_supplies)):
			_fly_to_army(a_army)
			a_army.get_node("SoundFromSupply").play()
			chain.remove_supplies(a_army.player)
			yield(get_tree().create_timer(GameDefs.battle_animation_delay), "timeout")
			a_army.resize(a_army.get_troops(), a_army.get_supply() + 1)


	if b_add_supplies > 0:
		# some troops became supplies
		for _i in range(b_add_supplies):
			_fly_from_army(b_army)
			b_army.get_node("SoundToSupply").play()
			yield(get_tree().create_timer(GameDefs.battle_animation_delay), "timeout")
			chain.add_supplies(b_army.player)
			if b_army.get_supply() > 0:
				b_army.resize(b_army.get_troops(), b_army.get_supply() - 1)
			else:
				b_army.resize(b_army.get_troops() - 1, 0)
	else:
		# some supplies taken from the supply chain
		for _i in range(abs(b_add_supplies)):
			_fly_to_army(b_army)
			b_army.get_node("SoundFromSupply").play()
			chain.remove_supplies(b_army.player)
			yield(get_tree().create_timer(GameDefs.battle_animation_delay), "timeout")
			b_army.resize(b_army.get_troops(), b_army.get_supply() + 1)


	yield(get_tree().create_timer(GameDefs.battle_animation_delay), "timeout")

	# scale up our armies for the conflict
	var a_survived = results[0] + results[1]
	var b_survived = results[2] + results[3]
	var rounds: int

	if a_survived == 0:
		# attacker destroyed
		rounds = a_army.get_total()
	elif b_survived == 0:
		# defender destroyed
		rounds = b_army.get_total()

	# trade blows until one is wiped out
	# sort the sprites so supplies poof before troops
	var a_death_order = _sort_for_death(a_army)
	var b_death_order = _sort_for_death(b_army)

	for i in range(rounds):
		# play the animation
		if b_death_order[i] != null:
			_troop_death_animation(b_army, b_death_order[i])
			yield(get_tree().create_timer(GameDefs.battle_animation_delay / 2.0 - 0.15), "timeout")
		if a_death_order[i] != null:
			_troop_death_animation(a_army, a_death_order[i])
			yield(get_tree().create_timer(GameDefs.battle_animation_delay / 2.0 + 0.15), "timeout")

	# celebrate
	var party_army = null
	if a_survived:
		party_army = a_army
	elif b_survived:
		party_army = b_army

	if party_army != null:
		yield(get_tree().create_timer(GameDefs.battle_animation_delay), "timeout")
		party_army.get_node("SoundDance").play()
		party_army.animation_name = "dance"
		# the end of the sound sends a signal to stop the dance animation
		yield(get_tree().create_timer(GameDefs.battle_animation_delay*2), "timeout")

	# end of animation delay
	yield(get_tree().create_timer(GameDefs.battle_animation_delay*2), "timeout")


static func _sort_for_death(army: Army)->Array:
	"sort the sprites so supplies poof before troops"
	var army_sprites = army._sprites
	var troop_spr = []
	var supplies_spr = []
	for spr in army_sprites:
		if spr.frames == SUPPLIES_FRAMES:
			supplies_spr.append(spr)
		else:
			troop_spr.append(spr)
	
	# add dummy entries for excess troops that we don't have room to display
	if army.get_total() > MAX_SPRITES:
		for _i in range(army.get_total() - MAX_SPRITES):
			supplies_spr.append(null)

	supplies_spr.invert()
	troop_spr.invert()
	return supplies_spr + troop_spr

func _fly_from_army(army: Army):
	var screen_pos = get_viewport().get_camera().unproject_position(army.global_transform.origin)
	# print("Supply animation: %d: %s, %d: %s" % [a_add_supplies, a_screen_pos, b_add_supplies, b_screen_pos])
	
	var spr = FlyingSprite.instance()
	get_viewport().add_child(spr)
	spr.do_flight_animation(
		army.frames, 
		screen_pos,
		Vector2(get_viewport().size.x, 0),
		GameDefs.battle_animation_delay
	)
	yield(get_tree().create_timer(GameDefs.battle_animation_delay), "timeout")

func _fly_to_army(army: Army):
	var screen_pos = get_viewport().get_camera().unproject_position(army.global_transform.origin)
	# print("Supply animation: %d: %s, %d: %s" % [a_add_supplies, a_screen_pos, b_add_supplies, b_screen_pos])
	
	var spr = FlyingSprite.instance()
	get_viewport().add_child(spr)
	spr.do_flight_animation(
		army.frames, 
		Vector2(get_viewport().size.x, 0),
		screen_pos,
		GameDefs.battle_animation_delay
	)
	yield(get_tree().create_timer(GameDefs.battle_animation_delay), "timeout")

func _troop_death_animation(army: Army, spr: ShadeSprite):
	if spr.frames == SUPPLIES_FRAMES:
		spr.frames = FIRE_SPRITES
		spr.animation_name = FIRE_ANIMATION_NAME
		army.get_node("SoundSupplyDie").play()
	else:
		spr.frames = POOF_SPRITES
		spr.animation_name = POOF_ANIMATION_NAME
		army.get_node("SoundTroopDie").play()

func _on_SoundDance_finished():
	# end our dance
	if animation_name == "dance":
		set_animation_name(DEFAULT_ANIMATION_NAME)
