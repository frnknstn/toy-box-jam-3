extends Node
class_name HexMap

"Hex helper tools"

const ROOT_THREE = 1.73205080757
const HEX_WD = ROOT_THREE
const HEX_WR = ROOT_THREE / 2.0
const Z_STEP: float = 1.5

const ODD_NEIGHBORS = [[1,0], [0,-1], [-1,-1], [-1,0], [-1,+1], [0,+1]]
const EVEN_NEIGHBORS = [[+1,0], [+1,-1], [0,-1], [-1,0], [0,+1], [+1,+1]]

const NEIGHBORS = [
	[[0,-1], [1,0], [0,1], [-1,1], [-1,0], [-1,-1]], # even neighbors
	[[1,-1], [1,0], [1,1], [0,1], [-1,0], [0,-1]], # odd neighbors
]

var map_resource: HexMapResource

var width: int
var height: int
var map_data

var nodes := []		# the {"elevation": 3, "sides": [...]} dicts from the resource
var node_at_coords := {}	# a lookup to resolve a 2-Array to a node index
var graph := AStar2D.new()

func _ready():
	pass


func hex_neighbor_coords(coords):
	"return a list of all possible hex coords adjacent to this one"
	var offset = (coords[1] % 2)
	var retval = NEIGHBORS[offset].duplicate()
	
	for i in range(len(retval)):
		retval[i] = [retval[i][0] + coords[0], retval[i][1] + coords[1]]
	return retval

func hex_connected_neighbors(coords:Array)->Array:
	"return a list of all hex coords that are reachable from this one"
	var retval = []
	var from = node_at_coords[coords]
	for hex in hex_neighbor_coords(coords):
		if not hex in node_at_coords:
			# hex not real
			continue
		var to = node_at_coords[hex]
		if graph.are_points_connected(from, to):
			retval.append(hex)
	return retval

func coord_to_pos(coord) -> Vector2:
	"convert hex coords to 2D world position vector"
	var col = coord[0]
	var row = coord[1]
	if (row % 2 == 0):
		return Vector2(col * HEX_WD, row * Z_STEP)
	else:
		return Vector2(col * HEX_WD + HEX_WR, row * Z_STEP)


func get_hex_at(coords):
	"return the hex details at the supplied 2-coord, or null"
	if (coords in node_at_coords):
		return nodes[node_at_coords[coords]]
	else:
		# no hex here
		return null


func load_resource(rn: String):
	print("Loading map '%s'" % rn)
	map_resource = load(rn)
	
	width = map_resource.map_width
	height = map_resource.map_height
	map_data = map_resource.data
	
	var graph_nodes := 0
	var graph_edges := 0
	graph.reserve_space(width * height)
	
	var hex_count := 0
	# parse the map
	for j in range(height):
		var row = map_data[j]
		for i in range(width):
			var coord = [i, j]
			var pos = coord_to_pos(coord)
			var hex = row[i]
			
			# populate our lists
			nodes.append(hex)
			node_at_coords[coord] = hex_count
			graph.add_point(hex_count, pos)
			graph_nodes += 1
			
			# link to neighbors in the graph
			for candidate in hex_neighbor_coords(coord):
				if (candidate in node_at_coords):
					var target_elevation = get_hex_at(candidate)["elevation"]
					var elevation_change = abs(target_elevation - hex["elevation"])
					if (elevation_change <=2) and (hex["elevation"] >= 2) and (target_elevation >= 2):
						# only go up and down by up to 2
						# don't go in the water
						graph.connect_points(hex_count, node_at_coords[candidate])
						graph_edges += 1
						
			
			hex_count += 1
	
	print("Graph has %d nodes and %d edges" % [graph_nodes, graph_edges])
 
