extends HBoxContainer

signal option_selected
signal assignment_cleared	# (self:Node)

export(int) var option_id
export(AtlasTexture) var icon setget set_icon
export(String) var text setget set_text
export(String) var status_hint

var assigned_value setget _set_assigned_value
var _is_assigned: bool = false

var _default_styles = {}
var _assigned_styles = {
	"hover": preload("res://resources/assignoption-box-assigned-hover.tres"),
	"pressed": null,
	"focus": null,
	"disabled": null,
	"normal": preload("res://resources/assignoption-box-assigned-normal.tres"),
}

func _ready():
	# record our default box styles
	_default_styles["hover"] = $Toggle.get_stylebox("hover")
	_default_styles["pressed"] = $Toggle.get_stylebox("pressed")
	_default_styles["focus"] = $Toggle.get_stylebox("focus")
	_default_styles["disabled"] = $Toggle.get_stylebox("disabled")
	_default_styles["normal"] = $Toggle.get_stylebox("normal")

func set_text(s):
	text = s.left(6)
	$Toggle.text = text

func set_icon(p_icon):
	icon = p_icon
	get_node("Toggle").icon = p_icon

func apply_box_styles(d: Dictionary):
	$Toggle.add_stylebox_override("hover", d["hover"])
	$Toggle.add_stylebox_override("pressed", d["pressed"])
	$Toggle.add_stylebox_override("focus", d["focus"])
	$Toggle.add_stylebox_override("disabled", d["disabled"])
	$Toggle.add_stylebox_override("normal", d["normal"])

func get_is_pressed():
	return $Toggle.pressed

func _set_assigned_value(val):
	assigned_value = val
	if val:
		_is_assigned = true
		apply_box_styles(_assigned_styles)
		$Toggle.pressed = false
	else:
		emit_signal("assignment_cleared", self)
		_is_assigned = false
		apply_box_styles(_default_styles)
	print("Assigned %s to option %d" % [val, option_id])


func _on_Toggle_toggled(pressed: bool):
	if pressed:
		emit_signal("option_selected", self)
		# clear any previous assignment
		if assigned_value != null:
			_set_assigned_value(null)
	else:
		pass

