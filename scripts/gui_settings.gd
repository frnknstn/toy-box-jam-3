extends Control

export(NodePath) var show_toggle_path
export(NodePath) var settings_window_path

# individual controls
export(NodePath) var uninvert_path
export(NodePath) var mute_path
export(NodePath) var fight_animations_path


var _is_ready := false

func _ready():
	update()
	show_window(false)
	_is_ready = true


func show_window(show: bool):
	if show:
		update()

	get_node(settings_window_path).visible = show

func update():
	"update the window based on the current state of the GameDefs"
	get_node(uninvert_path).pressed = GameDefs.uninvert_mouse_y
	get_node(mute_path).pressed = GameDefs.sound_mute
	get_node(fight_animations_path).pressed = GameDefs.fight_animations

func _on_ShowToggle_toggled(button_pressed: bool):
	show_window(button_pressed)

func _on_CloseButton_pressed():
	print("Close button pressed")
	get_node(show_toggle_path).pressed = false

func _on_Uninvert_toggled(button_pressed: bool):
	if _is_ready:
		GameDefs.uninvert_mouse_y = button_pressed
		GameDefs.save_settings()

func _on_Mute_toggled(button_pressed: bool):
	if _is_ready:
		GameDefs.sound_mute = button_pressed
		GameDefs.save_settings()

func _on_FightAnimations_toggled(button_pressed: bool):
	if _is_ready:
		GameDefs.fight_animations = button_pressed
		GameDefs.save_settings()

