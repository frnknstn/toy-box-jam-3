extends Node

const STATE = GameDefs.State
const MESSAGE = GameDefs.Message
const HEX_SPEC = GameDefs.HexSpec

var all_land = []
var land_shop = []

func _start():
	"called when entering a state"
	all_land = Game.get_all_usable_land()
	
	all_land.shuffle()

	for _i in range(3):
		land_shop.append(all_land.pop_back())

	Game.new_round()
	next_turn()

func _recv(message_type: int, details: Dictionary, _p_player=null):
	"called when we get a message"

	match message_type:
		MESSAGE.SUBMIT_HEX:
			# player has chosen a location
			var coords = details["coords"]
			var player = Game.active_player
			
			player.start_hexes.append(coords)
			Game.create_army(player, coords, 1, 0)

			# next turn
			Game.advance_player()
			if Game.active_player == null:
				Game.new_round()

			# check for end-of-state
			player = Game.active_player
			if len(player.start_hexes) != len(GameDefs.start_armies):
				# restock
				land_shop.erase(coords)
				land_shop.append(all_land.pop_back())
				next_turn()
			else:
				# we are done
				Game.call_deferred("change_game_state", STATE.PLACE_ARMY)

		_:
			print("UNHANDLED SERVER MESSAGE %d %s" % [ message_type, MESSAGE.keys()[message_type] ])
			assert(false)


func next_turn():
	Game.send_client_message(MESSAGE.REQUEST_HEX, Game.active_player.player_id, {
		"hex_spec": HEX_SPEC.ANY,
		"coords": land_shop,
	})


