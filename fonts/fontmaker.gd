extends Reference

tool


static func make():
	print("Generating fonts")
	make_font(7, 7, preload("res://fonts/morefonts_8.png"), 128, "font-8-medium.tres", 1)
	make_font(14, 14, preload("res://fonts/morefonts_16.png"), 256, "font-16-medium.tres", 2)
	make_font(21, 21, preload("res://fonts/morefonts_24.png"), 384, "font-24-medium.tres", 3)
	make_font(21, 21, preload("res://fonts/morefonts_24.png"), 384, "font-24-bold.tres", 3, 0, 4*24)

static func make_font(width: int, height: int, sheet, sheet_width: int, filename, gap=1, x_off:=0, y_off:=0):
	var font := BitmapFont.new()
	
	font.add_texture(sheet)
	font.height = height
	add_char(
		font, 
		" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_", 
		width, height, gap, sheet_width,
		x_off, y_off
	)
	add_char(
		font, 
		"abcdefghijklmnopqrstuvwxyz", 
		width, height, gap, sheet_width,
		x_off + width + gap, y_off + (height + gap) * 2
	)
	
	if ResourceSaver.save("res://fonts".plus_file(filename), font) != OK:
		print("Warning: font creation failed")


static func add_char(font, characters, width, height, gap, sheet_width, x_off: int=0, y_off: int=0):
	var x = x_off
	var y = y_off
	for c in characters:
		font.add_char(ord(c), 0, Rect2(x, y, width, height))
		x += width + gap
		if x >= sheet_width:
			x = 0
			y += height + gap
	
