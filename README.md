# Toy Box Jam 4 #

3D hex-based wargame with a vaguely HD2D aesthetic, made for Toy Box Jam 4

## Ideas #

## Todo #

* ~~camera controls~~
  * refined camera
* ~~mouse select hex~~
* hex highlighting
* ~~elevation routing~~
* gameplay
  * parties
  * placement
  * sides
  * movement
  * combat
    * supply
  * action selection
  * turn loop
  * win / loss

## Feature wishlist #

* zoom into action

## Technical Concepts #

### list of basic mid-level actions #

* lobby
  * server start
  * client connect
  * client kick
  * lobby message
  * game start
* game
  * general
    * client selects a region
    * client selects an adjacent region
    * client selects a value
  * setup
    * server sends map layout
    * server sends army spawns
  * region buy
    * server sends shortlist to client
  * turn planning
    * server sends turn planning parameters (?)
    * client sends list of plans
    * 
  * updates
    * server sends combat outcomes
    * server sends supply updates

## Gameplay Concepts #

* sound options
  * troop from supply
    * tbj3_sfx_39
  * troop to supply
    * tbj3_sfx_41 ?
    * tbj3_sfx_45
  * troop die
    * tbj3_sfx_46
  * supply die
    * tbj3_sfx_40
  * victory
    * dc_sfx_29 ?
    * dc_sfx_49
  * random UI ticks and clicks
    * sfxpack_7
    * sfxpack_8
* music options
  * into the belt
  * pastoral (maybe too high intensity)
  * eyes in the dark
  * flight of icarus (maybe too mystical)

Victory Points:

* 1 per region
* 1 per building
* 3 for most buildings

## Bugs (engine) #

* enabling anisotropic when importing a texture forces it to display as if filtered when loaded in HTML5
* the camera-tracking mesh instance to apply a postprocess effect with depth buffer (as per a tutorial on the site) clips about a 2 units in front of the camera
* that trick also does not work correctly with transparent objects (sprites or meshes)
* DOF does not work in HTML5
